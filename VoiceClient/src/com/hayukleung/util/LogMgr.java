package com.hayukleung.util;

import android.util.Log;

public class LogMgr
{
  private static final String TAG = "LogMgr-";

  // 是否打开Log日志输出 ，true打开，，false关闭
  public static boolean isLogOn = true;

  /*
   * 日志类型 
   */
  /* 调试日志类型 */
  public static final int DEBUG = 111;
  /* 错误日志类型 */
  public static final int ERROR = 112;
  /* 信息日志类型 */
  public static final int INFO = 113;
  /* 详细信息日志类型 */
  public static final int VERBOSE = 114;
  /* 警告调试日志类型 */
  public static final int WARN = 115;

  /* 显示，打印日志 */
  private static void showLog(final String tag, final String Message, final int Style)
  {
    if (isLogOn)
    {
      switch (Style)
      {
      case DEBUG:
        Log.d(TAG + tag, Message);
        break;
      case ERROR:
        Log.e(TAG + tag, Message);
        break;
      case INFO:
        Log.i(TAG + tag, Message);
        break;
      case VERBOSE:
        Log.v(TAG + tag, Message);
        break;
      case WARN:
        Log.w(TAG + tag, Message);
        break;
      }
    }
  }

  /**
   * 由于 logCat 每次最长只输出 4000 字符
   * 对于返回的较长的 JSON 字符串无法完全显示，故分段显示
   * 默认 info 级别
   * 
   * @param Message
   * @param logType
   */
  public static void writeLog(final String tag, final String Message, final int logType)
  {
    if (Message != null && !"".equalsIgnoreCase(Message))
    {
      int maxLogSize = 1000;
      for (int i = 0; i <= Message.length() / maxLogSize; i++)
      {
        int start = i * maxLogSize;
        int end = (i + 1) * maxLogSize;
        end = end > Message.length() ? Message.length() : end;
        showLog(tag, Message.substring(start, end), logType);
      }
    }
  }
}
