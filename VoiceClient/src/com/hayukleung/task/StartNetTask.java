package com.hayukleung.task;

import android.content.Context;

public class StartNetTask
{
  private Context context;
  // 后台执行
  private GetData getData;
  // 数据回调对象
  private UICallback uiCallback;

  public StartNetTask(Context context, GetData getData, UICallback uiCallback)
  {
    this.context = context;
    this.getData = getData;
    this.uiCallback = uiCallback;
  }

  /**
   * 数据请求方法
   * 
   * @param url
   *          请求地址
   * @param requestString
   *          请求参数
   * @param needTips
   *          是否需要提示
   * @param rescissible
   *          对话框是否可取消
   */
  public void requestData(String requestString, 
                           boolean needTips,
                           boolean rescissible)
  {
    NetTask task = new NetTask(context, 
                               needTips, 
                               rescissible,
                               getData,
                               uiCallback);
    task.execute(requestString);
  }

}
