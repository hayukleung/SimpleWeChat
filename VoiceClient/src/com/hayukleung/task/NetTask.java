﻿package com.hayukleung.task;

import com.hayukleung.util.LogMgr;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

class NetTask extends AsyncTask<String, Integer, String>
{
  private final static String TAG = "NetTask";
  // 上下文
  private Context context;
  // 是否需要提示
  private boolean needTips = true;
  // 提示框是否可以取消
  private boolean rescissible;
  // 提示框
  private ProgressDialog progressDlg;
  // 后台执行
  private GetData getData;
  // 数据回调对象
  private UICallback uiCallback;

  /**
   * 构造方法
   * 
   * @param context
   * @param url
   *          请求地址
   * @param needTips
   *          是否弹出对话框框
   * @param rescissible
   *          对话框是否可取消
   * @param uiCallback
   *          数据架设
   */
  public NetTask(Context context, 
                  boolean needTips, 
                  boolean rescissible,
                  GetData getData,
                  UICallback uiCallback)
  {
    LogMgr.writeLog(TAG, "NetTask()", LogMgr.INFO);
    this.context = context;
    this.getData = getData;
    this.uiCallback = uiCallback;
    this.needTips = needTips;
    this.rescissible = rescissible;
  }

  @Override
  protected String doInBackground(String... params)
  {
    LogMgr.writeLog(TAG, "doInBackground()", LogMgr.INFO);
    return getData.getData(params);
  }

  protected void onPostExecute(String result)
  {
    LogMgr.writeLog(TAG, "onPostExecute()", LogMgr.INFO);
    try
    {
      // 检查是否需要提示，再进行回调
      if (isDialogShowing())
      {
        uiCallback.callback(result);
      }
      if (needTips)
      {
        if (progressDlg != null)
        {
          progressDlg.dismiss();
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

  }

  /**
   * 检查对话框状态
   */
  private boolean isDialogShowing()
  {
    // 无需进行提示时，直接返回
    if (progressDlg == null)
    {
      return true;
    }
    // 对话框仍在提示时，直接返回
    if (progressDlg.isShowing())
    {
      return true;
    }
    return false;
  }

  /**
   * 该方法由 UI 线程进行调用，用户可以在这里尽情的访问 UI 组件
   * 很多时候，我们会在这里显示一个进度条，以示后台正在执行某项功能
   */
  protected void onPreExecute()
  {
    LogMgr.writeLog(TAG, "onPreExecute()", LogMgr.INFO);
    super.onPreExecute();
    try
    {
      // 初始化提示框，无需提示时，对话框为 Null
      if (needTips)
      {
        progressDlg = new ProgressDialog(context);
        progressDlg.setMessage("请稍等...");
        progressDlg.setCancelable(rescissible);
        progressDlg.show();
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

}
