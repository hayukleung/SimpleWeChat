package com.hayukleung.voiceserver;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import com.hayukleung.util.Constants;

public class UDPServer extends Thread
{
  DatagramPacket data;

  // constructor
  public UDPServer(DatagramPacket data)
  {
    this.data = data;
  }

  @Override
  public void run()
  { // ID of pool
    String string = new String(data.getData()).trim();

    Pool pool = (Pool) Server.pools.get(string);

    pool.ipSet.remove(string);

    if (pool.isPoolEnd)
    {
      Server.pools.remove(pool);
    }
  }

  public static void openServer() throws Exception
  {
    DatagramSocket socket = new DatagramSocket(Constants.SERVER_UDP_PORT_INT);

    while (true)
    {
      byte[] bytes = new byte[1 * Constants.K];
      DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
      socket.receive(packet);
      new UDPServer(packet).start();
    }
  }
}
