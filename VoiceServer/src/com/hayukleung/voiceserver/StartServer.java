package com.hayukleung.voiceserver;

public class StartServer
{
  public static void main(String[] args)
  {
    // start TCP server
    new Thread()
    {
      @Override
      public void run()
      {
        super.run();
        try
        {
          Server.openServer();
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    }.start();
    // start UDP server
    new Thread()
    {
      @Override
      public void run()
      {
        super.run();
        try
        {
          UDPServer.openServer();
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    }.start();
  }
}
